﻿using Ion.Data.Networking.Logging;
using Ion.Data.Networking.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager
{
    public class DataReceiver : IService
    {
        public DataClient ReceiveClient { get; set; }
        public int PackageCount { get; private set; }
        Task task;
        ILogger logger;
        bool running = true;

        public Service Container { get; set; }

        public void Initialize(Service container)
        {
            this.logger = container.Logger;
        }

        public void Start()
        {
            ReceiveClient = new LocalNetworkClient(160) { ShortMode = true };
            task = Task.Run(new Action(ReceiveLoop));
        }

        public void ReceiveLoop()
        {
            if (ReceiveClient == null)
            {
                Container.Crash(this, $"{nameof(ReceiveClient)} is not available");
                return;
            }
            try
            {
                while (running)
                {
                    DataWrapper[] data = ReceiveClient.ReadDataWrappers(false);
                    PackageCount++;
                    SensorDataStore.AddValue(data);
                }

            }
            catch (Exception e)
            {
                logger.WriteException("Exception", e, 4);
            }
        }

        public void Stop()
        {
            running = false;
        }
    }
}
