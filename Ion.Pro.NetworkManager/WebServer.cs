﻿using Ion.Data.Networking.CoreCom;
using Ion.Data.Networking.Logging;
using NicroWare.Lib.NetUtility.Html;
using NicroWare.Lib.NetUtility.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace Ion.Pro.NetworkManager
{
    public class WebServer : IService
    {
        TcpListener listener;
        ILogger logger;

        //public const string HtmlBase = "../../HTML/";
        public const string HtmlBase = "HTML/";
        public Dictionary<string, Controller> controllers = new Dictionary<string, Controller>();

        public Service Container { get; set; }

        public WebServer(int port)
            : this(IPAddress.Any, port)
        {
        }

        public WebServer(IPAddress bindAddress, int port)
        {
            listener = new TcpListener(bindAddress, port);
            controllers.Add("WEB", new WebController() { Server = this });
            controllers.Add("FILE", new FileController() { Server = this });
        }

        public void Initialize(Service container)
        {
            this.logger = container.Logger;
        }

        public void Start()
        {
            listener.Start();
            AcceptClient();
            Process.Start("http://127.0.0.1:9966");
        }

        private async void AcceptClient()
        {
            TcpClient client = await listener.AcceptTcpClientAsync();

            AcceptClient();
            if (client != null)
            {
                try
                {
                    logger.WriteInfo("Got client!");
                    HandleClient(client);
                }
                catch (OperationCanceledException e)
                {
                    logger.WriteException("Empty client exception", new OperationCanceledException("No data was available"), 0);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    logger.WriteException("Exception", e, 3);
                }
            }
            else
            {
                logger.WriteInfo("Got null client");
            }
        }

        private void HandleClient(TcpClient client)
        {
            bool forcedExit = false;
            Stream networkStream = client.GetStream();
            //File.OpenRead(@"C:\Users\Nicolas\Desktop\calender template.xlsx")
            //ProtocolReader preader = new ProtocolReader(networkStream);
            //byte[] test = preader.ReadBytes(15000);
            HttpReader reader = new HttpReader(networkStream);
            HttpHeaderRequest header = reader.ReadMessage();
            if (header.AllFiles.Count > 0)
            {
                foreach (HttpFileInfo files in header.AllFiles)
                {
                    //TODO: Make it write to TEMP folder
                    File.WriteAllBytes(files.Name, files.RawData);
                    FileInfo fi = new FileInfo(files.Name);
                    if (fi.Extension == ".zip")
                    {
                        ZipArchive zip = new ZipArchive(fi.OpenRead());
                        ZipArchiveEntry zae = zip.GetEntry("update.conf");
                        if (zae != null)
                        {
                            if (Program.CoreOnline)
                            {
                                CommandSender sender = new CommandSender();
                                sender.SendCommand("Update", new string[] { fi.FullName });
                                sender.Close();
                                logger.WriteInfo("Started Updating");
                                forcedExit = true;
                            }
                            else
                            {
                                logger.WriteInfo("Updating is not available sins the core is not running");
                            }
                        }
                    }
                }
            }
            logger.WriteInfo("Requested: " + header.FullPath);
            //reader.Close();
            HtmlWriter writer = new HtmlWriter(networkStream);
            string text = File.ReadAllText(HtmlBase + "index.html");
            string[] path = header.BasePath.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (path.Length > 0 && controllers.ContainsKey(path[0].ToUpper()))
            {
                if (path.Length > 1 && controllers[path[0].ToUpper()].AllActions.ContainsKey(path[1].ToUpper()))
                {
                    text = controllers[path[0].ToUpper()].AllActions[path[1].ToUpper()](text, path, writer);
                }
                else
                {
                    text = text.Replace("@Content", "<h3>Overview</h3>");
                }
            }
            if (text != null)
                writer.WriteHtml(text);
            writer.Close();
            if (forcedExit)
            {
                logger.WriteInfo("Terminating NetworkManager");
                Environment.Exit(0);
            }
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }
    }

    public delegate string CtrlAction(string input, string[] path, HtmlWriter writer);

    public class Controller
    {
        public Dictionary<string, CtrlAction> AllActions { get; set; }
        public WebServer Server { get; set; }

        public Controller()
        {
            AllActions = new Dictionary<string, CtrlAction>();
            Type t = GetType();
            MethodInfo[] methods = t.GetMethods();
            foreach (MethodInfo mi in methods)
            {
                Delegate d = Delegate.CreateDelegate(typeof(CtrlAction), this, mi, false);
                if (d != null)
                    AllActions.Add(mi.Name.ToUpper(), d as CtrlAction);
            }
        }
    }

    public class WebController : Controller
    {
        public string Overview(string input, string[] path, HtmlWriter writer)
        {
            if (Program.CoreOnline)
            {
                CommandSender sender = new CommandSender();
                string version = sender.SendCommand("Version", new string[0]);
                sender.Close();
                return input.Replace("@Content", "<h3>Overview</h3><br /><div style='text-align: left;'>NetworkManager version: " + Program.Version + "<br />Core version: " + version + "</div>");
            }
            else
            {
                return input.Replace("@Content", "<h3>Overview</h3><br /><div style='text-align: left;'>NetworkManager version: " + Program.Version + "<br />Core offline</div>");
            }
        }

        public string Log(string input, string[] path, HtmlWriter writer)
        {
            HtmlBuilder builder = new HtmlBuilder();
            int startIndex;
            if (!(path.Length > 2 && int.TryParse(path[2], out startIndex)))
            {
                startIndex = 0;
            }
            HtmlTable table = new HtmlTable();
            table.AddHeader("Time", "Sender", "Level", "Info");
            table.AddHeaderParams("style=\"180px\"", "style=\"180px\"", "style=\"180px\"", "style=\"80px\"");
            foreach (LogEntry entry in Program.TestManager.ListEntries())
            {
                if (entry.Level >= startIndex)
                    table.AddRow(entry.Time.ToString(), entry.Sender.ToString(), entry.Level.ToString(), entry.Value.Replace("\n", "<br />"));
            }
            builder.BaseNode = table.ToDom();
            return input.Replace("@Content", "<h3>Log</h3><a href=\"/web/log/0\" style=\"padding: 10px; \">All</a><a href=\"/web/log/1\" style=\"padding: 10px; \">Info</a><a href=\"/web/log/3\" style=\"padding: 10px; \">Errors</a><br />" + builder.ToString());
        }

        public string File(string input, string[] path, HtmlWriter writer)
        {
            string dirPath = "/";

            int preRemove = 1;
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                preRemove = 3;
            for (int i = 2; i < path.Length; i++)
            {
                dirPath += path[i] + "/";
            }
            DirectoryInfo test = new DirectoryInfo(dirPath);
            if (!test.Exists)
            {
                FileInfo fileTest = new FileInfo(dirPath.Remove(dirPath.Length - 1, 1));
                if (fileTest.Exists)
                {

                    writer.WriteFile(fileTest.FullName);
                    //test = fileTest.Directory;
                    return null;
                }
            }
            HtmlTable dirTable = new HtmlTable();
            dirTable.AddHeader("Name", "Date modified", "Type", "Size");
            dirTable.AddRow("<a href='/web/file/" + (test.Parent?.FullName.Remove(0, preRemove).Replace('\\', '/') ?? "") + "'>..</a>", "", "Up", "");
            foreach (DirectoryInfo di in test.GetDirectories())
            {
                dirTable.AddRow("<a href='/web/file/" + di.FullName.Remove(0, preRemove).Replace('\\', '/') + "'>" + di.Name + "</a>", di.LastWriteTime.ToString(), "File folder", "");
            }
            foreach (FileInfo di in test.GetFiles())
            {
                dirTable.AddRow("<a href='/web/file/" + di.FullName.Remove(0, preRemove).Replace('\\', '/') + "'>" + di.Name + "</a>", di.LastWriteTime.ToString(), di.Extension + " file", (di.Length / 1024) + " KB");
            }
            HtmlBuilder dirBuilder = new HtmlBuilder();
            dirBuilder.BaseNode = dirTable.ToDom();
            return input.Replace("@Content", "<h3>Explorer</h3><br /><span>Current dir: " + test.FullName + "</span><br />" + dirBuilder.ToString());
        }

        public string Diagnostic(string input, string[] path, HtmlWriter writer)
        {
            Process[] allProcesses = Process.GetProcesses();
            HtmlTable procTable = new HtmlTable();
            procTable.AddHeader("Id", "Process Name", "Main Window Title", "State", "Total Time");
            foreach (Process p in allProcesses)
            {
                try
                {
                    procTable.AddRow(p.Id.ToString(), p.ProcessName, p.MainWindowTitle, p.Responding ? "running" : "crashed", p.TotalProcessorTime.ToString());
                }
                catch
                { }
            }
            HtmlBuilder procTemp = new HtmlBuilder();
            procTemp.BaseNode = procTable.ToDom();
            return input.Replace("@Content", "<h3>Diagnostic</h3> <br/> If you see this page, the web server is running :P </br>" + procTemp.ToString(false));
        }

        public string Update(string input, string[] path, HtmlWriter writer)
        {
            if (Program.CoreOnline)
                return input.Replace("@Content", "<h3>Update</h3>" + System.IO.File.ReadAllText(WebServer.HtmlBase + "update.html"));
            else
                return input.Replace("@Content", "<h3>Update</h3>Core not available");
        }

        public string Services(string input, string[] path, HtmlWriter writer)
        {
            HtmlTable table = new HtmlTable();
            table.AddHeader("Name", "Status");
            foreach (Service s in Server.Container.Manager.AllServices)
            {
                table.AddRow(s.ServiceObject.GetType().Name, s.State.ToString());
            }

            return input.Replace("@Content", "<h3>Network manager services</h3> <br />" + (new HtmlBuilder() { BaseNode = table.ToDom() }).ToString());
        }
    }

    public class FileController : Controller
    {
        public string Browse(string input, string[] path, HtmlWriter writer)
        {
            string dirPath = "/";


            for (int i = 2; i < path.Length; i++)
            {
                dirPath += path[i] + "/";
            }
            DirectoryInfo test = new DirectoryInfo(dirPath);
            if (!test.Exists)
            {
                FileInfo fileTest = new FileInfo(dirPath.Remove(dirPath.Length - 1, 1));
                if (fileTest.Exists)
                {

                    writer.WriteFile(fileTest.FullName);
                    //test = fileTest.Directory;
                    return null;
                }
            }
            HtmlTable dirTable = new HtmlTable();
            dirTable.AddHeader("Name", "Date modified", "Type", "Size", "Actions");

            dirTable.AddRow(BrowseLink(test.Parent == null ? "" : FormatPath(test.Parent.FullName), ".."), "", "Up", "", "");
            foreach (DirectoryInfo di in test.GetDirectories())
            {
                string tempDirPath = FormatPath(di.FullName);
                dirTable.AddRow(BrowseLink(tempDirPath, di.Name), di.LastWriteTime.ToString(), "File folder", "", DeleteLink(tempDirPath));
            }
            foreach (FileInfo fi in test.GetFiles())
            {
                string tempFilePath = FormatPath(fi.FullName);
                dirTable.AddRow(DownloadLink(tempFilePath, fi.Name), fi.LastWriteTime.ToString(), fi.Extension + " file", PritifySize(fi.Length), DeleteLink(tempFilePath));
            }
            HtmlBuilder dirBuilder = new HtmlBuilder();
            dirBuilder.BaseNode = dirTable.ToDom();
            return input.Replace("@Content", "<h3>Explorer</h3><br /><span>Current dir: " + test.FullName + "</span><br />" + dirBuilder.ToString());
        }

        public string Down(string input, string[] path, HtmlWriter writer)
        {
            string dirPath = "/";

            for (int i = 2; i < path.Length; i++)
            {
                dirPath += path[i] + "/";
            }

            FileInfo fileTest = new FileInfo(dirPath.Remove(dirPath.Length - 1, 1));
            if (fileTest.Exists)
            {

                writer.WriteFile(fileTest.FullName);
                //test = fileTest.Directory;
                return null;
            }
            return input.Replace("@Content", "<h3>404 File not found</h3>");
        }

        public string Delete(string input, string[] path, HtmlWriter writer)
        {
            return input.Replace("@Content", "<h3>Not yet impementet</h3>");
        }

        private string FormatPath(string path)
        {
            int preRemove = 1;
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
                preRemove = 3;
            return path.Remove(0, preRemove).Replace('\\', '/');
        }

        private string BrowseLink(string path, string name)
        {
            return ActionLink("browse", path, name);
        }

        private string DownloadLink(string path)
        {
            return DownloadLink(path, "Download");
        }

        private string DownloadLink(string path, string name)
        {
            return ActionLink("down", path, name);
        }

        private string DeleteLink(string path)
        {
            return ActionLink("delete", path, "Delete");
        }

        private string ActionLink(string action, string path, string name)
        {
            return string.Format("<a href='/file/{0}/{1}' >{2}</a>", action, path, name);
        }

        private string PritifySize(long size)
        {
            int counter = 0;
            while (size > 1024 && counter < 5)
            {
                size /= 1024;
                counter++;
            }
            if (counter > 0)
                size += 1;
            string prefix = "B";
            switch (counter)
            {
                case 1:
                    prefix = "KB";
                    break;
                case 2:
                    prefix = "MB";
                    break;
                case 3:
                    prefix = "GB";
                    break;
                case 4:
                    prefix = "TB";
                    break;
            }

            return size.ToString() + " " + prefix;
        }
    }

    public class HtmlTable
    {
        string[] headers = null;
        string[] pars = null;
        List<string[]> rows = new List<string[]>();

        public string ID { get; set; } = "";
        public string TableParams { get; set; } = "";

        public HtmlTable()
        {

        }

        public void AddHeaderParams(params string[] pars)
        {
            this.pars = pars;
        }

        public void AddHeader(params string[] cols)
        {
            this.headers = cols;
        }

        public void AddRow(params string[] cols)
        {
            this.rows.Add(cols);
        }

        public HtmlNode ToDom()
        {
            HtmlNode table = new HtmlNode("table", TableParams);
            if (headers != null)
            {
                HtmlNode trHead = new HtmlNode("tr");
                for (int i = 0; i < headers.Length; i++)
                {
                    trHead.Childern.Add(new HtmlNode("th", pars?.Length > i ? pars[i] : "" ?? "", new TextNode(headers[i])));
                }
                table.Childern.Add(trHead);
            }
            foreach (string[] row in rows)
            {
                HtmlNode trData = new HtmlNode("tr");
                foreach (string s in row)
                {
                    trData.Childern.Add(new HtmlNode("td", new TextNode(s)));
                }
                table.Childern.Add(trData);
            }
            return table;
        }
    }
}
