﻿using Ion.Data.Networking.Manager;
using Ion.Data.Sensor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager
{
    public static class SensorDataStore
    {
        public static Dictionary<SensorLookup, DataWrapper> Latest { get; private set; } = new Dictionary<SensorLookup, DataWrapper>();
        public static event SensorUpdateEventHandler SensorUpdate;

        public static void AddValue(DataWrapper[] wrapper)
        {
            foreach (DataWrapper dw in wrapper)
            {
                AddValue(dw);
            }
        }

        public static void AddValue(DataWrapper wrapper)
        {
            wrapper.TimeStamp = (int)GlobalTime.ElapsedTime.TotalMilliseconds;
            SensorLookup lookup = SensorLookup.GetById(wrapper.SensorID);
            if (Latest.ContainsKey(lookup))
                Latest[lookup] = wrapper;
            else
                Latest.Add(lookup, wrapper);
            SensorUpdate?.Invoke(null, new SensorUpdateEventArgs(wrapper));
        }

        public static DataWrapper? GetData(SensorLookup lookup)
        {
            if (lookup == null)
                return null;
            if (Latest.ContainsKey(lookup))
                return Latest[lookup];
            return null;
        }

        public static DataWrapper? GetData(ushort sensorID)
        {
            return GetData(SensorLookup.GetById(sensorID));
        }

        public static DataWrapper? GetData(string name)
        {
            return GetData(SensorLookup.GetByName(name));
        }
    }

    public class SensorUpdateEventArgs : EventArgs
    {
        public DataWrapper Data { get; private set; }

        public SensorUpdateEventArgs(DataWrapper data)
        {
            Data = data;
        }
    }

    public delegate void SensorUpdateEventHandler(object sender, SensorUpdateEventArgs e);

    public class ObjectWrap<T>
    {
        public T Value { get; set; }

        public ObjectWrap(T value)
        {
            this.Value = value;
        }
    }
}
