﻿using Ion.Data.Networking.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.NetworkManager
{
    public class ScreenUpdater : IService
    {
        //UdpClient sender = new UdpClient();
        DataClient sender;
        public Service Container { get; set; }

        public void Initialize(Service container)
        {
            //sender.Connect("127.0.0.1", 9960);
            sender = new LocalNetworkClient(162) { ShortMode = true };
            (sender as LocalNetworkClient).Connect(IPAddress.Loopback, 163);
        }

        public void Start()
        {
            SensorDataStore.SensorUpdate += SensorDataStore_SensorUpdate;
        }

        private void SensorDataStore_SensorUpdate(object sender, SensorUpdateEventArgs e)
        {
            byte[] bytes = e.Data.GetBytesWithTime();
            this.sender.SendDataWrappers(new DataWrapper[] { e.Data } );//.Send(bytes, bytes.Length);
        }

        public void Stop()
        {
            SensorDataStore.SensorUpdate -= SensorDataStore_SensorUpdate;
        }
    }
}
