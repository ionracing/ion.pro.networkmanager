﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NicroWare.Lib.NetUtility.Http;
using NicroWare.Lib.NetUtility.Html;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Ion.Data.Networking.Logging;
using System.Diagnostics;
using System.Reflection;
using Ion.Data.Networking.CoreCom;

namespace Ion.Pro.NetworkManager
{

    /*
    +-----------+-----------+-------------------+
    |Port       |Protocol   |Usage              |
    +-----------+-----------+-------------------+
    |9966       |TCP        |HTTP               |
    |9965       |UDP        |Log Listner (Core) |
    |9965       |TCP        |Log Sender  (Core) |
    |9960       |UDP        |Sensor Data Stream |
    |9950       |TCP        |CoreCom     (Core) |
    +-----------+-----------+-------------------+
    */
    class Program
    {
        //public static ILogManager TestManager = new LogManager();
        public static ILogManager TestManager;// = new LogManagerClient();
        public static bool Running { get; set; } = true;
        public const string Version = "0.0.7 alpha";
        public const string Name = "NETMAN";
        public static bool CoreOnline { get; private set; } = false;

        static void Main(string[] args)
        {
            Console.Title = "Network Manager";
            Process p = Process.GetCurrentProcess();
            try
            {
                CommandSender sender = new CommandSender();
                string value = sender.SendCommand("Register", Name, p.Id.ToString());
                sender.Close();
                CoreOnline = true;
            }
            catch
            { }

            if (CoreOnline)
            {
                TestManager = new LogManagerClient();
            }
            else // Creates a logger in case the IonCore is not responding.
            {
                TestManager = new LogManager();
            }
            
            ILogger system = TestManager.CreateLogger("SYSTEM");
            if (!CoreOnline) system.WriteInfo("IonCore not available");
            system.WriteInfo("Starting System");


            ServiceManager manager = new ServiceManager(TestManager);
            manager.Add(new WebServer(9966));
            manager.Add(new DataReceiver());
            manager.Add(new ScreenUpdater());
            manager.Initialize();
            manager.Start();
            manager.WaitForExit();
            //TestManager.OnInfo += TestManager_OnInfo;

            /*system.WriteInfo("Creating Server");
            WebServer server = new WebServer(9966);
            system.WriteInfo("Starting Server");
            server.Start();
            while (Running)
            {
                System.Threading.Thread.Sleep(1000);
            }*/
        }

        private static void TestManager_OnInfo(object sender, LogWriteEventArgs e)
        {
            Console.WriteLine($"{e.Entry.Time} ({e.Entry.Sender})[{e.Entry.Level}] {e.Entry.Value}");
        }
    }


}
